export class User {
  constructor(public email:     string,
              public name:      string,
              private password: string){}

  matches(another: User): boolean {
    return another          !== undefined
        && another.email    === this.email
        && another.password === this.password
  }
}

export const users: {[key: string]: User}= {
  "murilovictor63@gmail.com": new User('murilovictor63@gmail.com', 'Murilo', 'mvictor'),
  "murilomoura@gmail.com": new User('murilomoura@gmail.com', 'Moura', 'mmoura')
}
