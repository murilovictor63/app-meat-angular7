import { Request, Response } from 'express'
import { User, users } from './users'
import { apiConfig } from './api-config'
import * as jwt from 'jsonwebtoken'

export const handleAuthenication = (req: Request, resp: Response) => {
  const user: User = req.body
  if(isValid(user)){
    const dbUser = users[user.email]

    // Obtendo Token
    const token = jwt.sign({sub: dbUser.email, iss: 'meat-api'}, apiConfig.secret)

    resp.json({name: dbUser.name, email: dbUser.email, accessToken: token})
  }else{
    resp.status(403).json({message: 'Usuário ou Senha inválidos.'})
  }
}

function isValid(user: User): boolean {
  if(!user){
    return false
  }
  const dbUser = users[user.email]
  return dbUser !== undefined && dbUser.matches(user)
}
