"use strict";
exports.__esModule = true;
var User = (function () {
    function User(email, name, password) {
        this.email = email;
        this.name = name;
        this.password = password;
    }
    User.prototype.matches = function (another) {
        return another !== undefined
            && another.email === this.email
            && another.password === this.password;
    };
    return User;
}());
exports.User = User;
exports.users = {
    "murilovictor63@gmail.com": new User('murilovictor63@gmail.com', 'Murilo', 'mvictor'),
    "murilomoura@gmail.com": new User('murilomoura@gmail.com', 'Moura', 'mmoura')
};
