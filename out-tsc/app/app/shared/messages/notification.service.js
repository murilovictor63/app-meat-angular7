import { EventEmitter } from "@angular/core";
var NotificationService = /** @class */ (function () {
    function NotificationService() {
        this.nofitier = new EventEmitter();
    }
    NotificationService.prototype.notify = function (message) {
        this.nofitier.emit(message);
    };
    return NotificationService;
}());
export { NotificationService };
//# sourceMappingURL=notification.service.js.map