import { HomeComponent } from './home/home.component';
import { RestaurantsComponent } from "app/restaurants/restaurants.component";
import { RestaurantDetailComponent } from "app/restaurant-detail/restaurant-detail.component";
import { MenuComponent } from "app/restaurant-detail/menu/menu.component";
import { ReviewsComponent } from "app/restaurant-detail/reviews/reviews.component";
import { OrderSummaryComponent } from "app/order-summary/order-summary.component";
import { NotFoundComponent } from "app/not-found/not-found.component";
import { LoginComponent } from "app/security/login/login.component";
import { LoggedInGuard } from "app/security/loggedin.guard";
export var ROUTES = [
    { path: '', component: HomeComponent },
    { path: 'login/:to', component: LoginComponent },
    { path: 'login', component: LoginComponent },
    { path: 'about', loadChildren: './about/about.module#AboutModule' },
    { path: 'restaurants/:id', component: RestaurantDetailComponent,
        children: [
            { path: '', redirectTo: 'menu', pathMatch: 'full' },
            { path: 'menu', component: MenuComponent },
            { path: 'reviews', component: ReviewsComponent }
        ] },
    { path: 'restaurants', component: RestaurantsComponent },
    { path: 'order', canLoad: [LoggedInGuard], canActivate: [LoggedInGuard],
        loadChildren: './order/order.module#OrderModule' },
    { path: 'order-summary', component: OrderSummaryComponent },
    { path: '**', component: NotFoundComponent } // WildCard
];
//# sourceMappingURL=app.routes.js.map