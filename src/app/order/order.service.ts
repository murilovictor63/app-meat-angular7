import { Injectable } from "@angular/core";
import { ShoppingCartService } from "app/restaurant-detail/shopping-cart/shopping-cart.service";
import { CartItem } from "app/restaurant-detail/shopping-cart/cart-item.model";
import { Order } from "app/order/order.model";
import { Observable } from "rxjs";
// import { Http, Headers, RequestOptions} from "@angular/http";
import { HttpClient, HttpHeaders} from '@angular/common/http'
import { MEAT_API } from '../app.api'
import {map} from 'rxjs/operators'
import { LoginService } from "app/security/login/login.service";



@Injectable()
export class OrderService {
  constructor(private cartService: ShoppingCartService,
              private http: HttpClient){}

  itemsValueService(): number {
    return this.cartService.total()
  }

  cartItems(): CartItem[]{
    return this.cartService.itens
  }

  increaseQty(item: CartItem){
    this.cartService.increaseQty(item)
  }

  decreaseQty(item: CartItem){
    this.cartService.decreaseQty(item)
  }

  removeItem(item: CartItem){
    this.cartService.removeItem(item)
  }

  checkOrderService(order: Order): Observable<string> {
    return this.http.post<Order>(`${MEAT_API}/orders`, order)
                    .pipe(map(order => order.id))
  }

  clear(){
    this.cartService.clearAll()
  }

}
