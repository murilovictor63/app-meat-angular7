import { HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { ErrorHandler, Injectable, Injector, NgZone } from "@angular/core";
import { NotificationService } from "app/shared/messages/notification.service";
import { LoginService } from "app/security/login/login.service";

@Injectable()
export class ApplicantionErrorHandler extends ErrorHandler {

  constructor(private ns: NotificationService,
              private injector: Injector,
              private zone: NgZone){
    super()
  }

  handleError(errorResponse: HttpErrorResponse | any){
    if(errorResponse instanceof HttpErrorResponse){
      const message = errorResponse.message
      console.log(message)
      this.zone.run(()=>{  // Criando uma ZONA. EX. quando um codigo execulta mais o angular não consegue perceber
        switch(errorResponse.status){
          case 401:
            this.injector.get(LoginService).handleLogin()
            this.ns.notify('Redirecionado para página de Login.')
            break;
          case 403:
            this.ns.notify('Não autorizado.')
            break;
          case 404:
            this.ns.notify('Recurso não encontrado. Verifique o console.')
            break;
        }
      })
    }
    super.handleError(errorResponse)
  }
}
