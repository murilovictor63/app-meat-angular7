import { Component, OnInit } from '@angular/core';
import { Restaurant } from "../restaurants/restaurant/restaurant.model";
import { RestaurantService } from "app/restaurants/restaurants.service";
import { trigger, state, style, transition, animate } from '@angular/animations'
import { FormBuilder,  FormControl, FormGroup } from '@angular/forms'
import { Observable, from } from 'rxjs'
// import 'rxjs/add/operator/switchMap'
// import 'rxjs/add/operator/do'
// import 'rxjs/add/operator/debounceTime'
// import 'rxjs/add/operator/distinctUntilChanged'
// import 'rxjs/add/operator/catch'

import {switchMap, tap, debounceTime, distinctUntilChanged, catchError} from 'rxjs/operators'



@Component({
  selector: 'mt-restaurants',
  templateUrl: './restaurants.component.html',
  animations: [
    trigger('toggleSearch', [
      state('hidden', style({
        opacity: 0,
        "max-height": "0px"
      })),
      state('visible', style({
        opacity: 1,
        "max-height": "70px",
        "margin-top": "20px"
      })),
      transition('* => *', animate('250ms 0s ease-in-out'))
    ])
  ]
})

export class RestaurantsComponent implements OnInit {

  searchBarState = 'hidden'
  restaurants: Restaurant[]

  searchForm: FormGroup
  searchControl: FormControl

  constructor(private restaurantsService: RestaurantService,
              private fb: FormBuilder) { }

  ngOnInit() {

    this.searchControl = this.fb.control('')
    this.searchForm = this.fb.group({
      searchControl: this.searchControl
    })

    //this.searchControl.valueChanges.subscribe(searchTerm => console.log(searchTerm))

    this.searchControl.valueChanges
        .pipe(
              debounceTime(500),    // aguarda 500ms para realizar uma requisição
              distinctUntilChanged(), // Não deixa enviar duas requisições iguais ex. doces doces
              switchMap(searchTerm =>
                this.restaurantsService.restaurants(searchTerm)
                .pipe(catchError(error => from([]))))
        ).subscribe(rests => this.restaurants = rests)

    this.restaurantsService.restaurants()
                           .subscribe(rests => this.restaurants = rests)

  }

  toggleSearch(){
    this.searchBarState = this.searchBarState === 'hidden' ? 'visible': 'hidden'
  }

}
