import { Restaurant } from "app/restaurants/restaurant/restaurant.model";
import { Injectable } from "@angular/core";
import { HttpClient, HttpParams} from '@angular/common/http'

import { MEAT_API } from '../app.api'
import { Observable } from 'rxjs'
import { MenuItem } from "app/restaurant-detail/menu-item/menu-item-model";


@Injectable()
export class RestaurantService {

  constructor(private http: HttpClient){}


restaurants(search?: string): Observable<Restaurant[]>{
    let params: HttpParams = undefined
    if(search){
      params = new HttpParams().set('q', search) //.append
    }
    return this.http.get<Restaurant[]>(`${MEAT_API}/restaurants`, {params: params})
  }

restaurantById(id: String): Observable<Restaurant>{
    return this.http.get<Restaurant>(`${MEAT_API}/restaurants/${id}`)
}

reviewsOfRestaurant(id: String): Observable<any>{
    return this.http.get<Restaurant>(`${MEAT_API}/restaurants/${id}/reviews`)
}


menuOfRestaurant(id:String): Observable<MenuItem[]>{
  return this.http.get<MenuItem[]>(`${MEAT_API}/restaurants/${id}/menu`)
}


// restaurants(search?: string): Observable<Restaurant[]>{
//     return this.http.get(`${MEAT_API}/restaurants`, {params: {q: search}})
//       .map(response => response.json())
//       .catch(ErrorHandler.handlerError)
//   }
//
// restaurantById(id: String): Observable<Restaurant>{
//     return this.http.get(`${MEAT_API}/restaurants/${id}`)
//       .map(response => response.json())
//       .catch(ErrorHandler.handlerError)
// }
//
// reviewsOfRestaurant(id: String): Observable<any>{
//     return this.http.get(`${MEAT_API}/restaurants/${id}/reviews`)
//       .map(response => response.json())
//       .catch(ErrorHandler.handlerError)
// }
//
//
// menuOfRestaurant(id:String): Observable<MenuItem[]>{
//   return this.http.get(`${MEAT_API}/restaurants/${id}/menu`)
//     .map(response => response.json())
//     .catch(ErrorHandler.handlerError)
// }

  // rests: Restaurant[] = [
  //     {
  //       id: "bread-bakery",
  //       name: "Bread & Bakery",
  //       category: "Bakery",
  //       deliveryEstimate: "25m",
  //       rating: 4.9,
  //       imagePath: "assets/img/restaurants/breadbakery.png"
  //     },
  //     {
  //       id: "burger-house",
  //       name: "Burger House",
  //       category: "Hamburgers",
  //       deliveryEstimate: "100m",
  //       rating: 3.5,
  //       imagePath: "assets/img/restaurants/burgerhouse.png"
  //     }
  // ]
  //
  // restaurants(): Restaurant[]{
  //   return this.rests
  // }
}
