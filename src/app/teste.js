const text = `1. 7COMM enviar e-mail para: rh@7comm.com.br
              2. AABB (CLUBE SOCIAL) enviar e-mail para: rh@aabb.esp.br
              3. ALGAR enviar e-mail para: talentoshumanos@algar.com.br
              4. APOLO enviar e-mail para: rh@tubosapolo.com.br
              5. ARTEB enviar e-mail para: selecao@arteb.com.br
              6. ARTHA enviar e-mail para: rh@arthabr.com
              7. AZALÉIA enviar e-mail para: rh@azaleia.com.br
              8. BASF enviar e-mail para: recursos.humanos@basf-sa.com.br
              9. BOM BRIL enviar e-mail para: selecao@bombril.com.br
              10. BOSCH enviar e-mail para: recruta.bosch.rbbr@br.bosch.com
              11. BOUCINHAS enviar e-mail para: rhboucin@boucinhas.com.br
              12. BRAHMA enviar e-mail para: gente@brahma.com.br
              13. BRASILATA enviar e-mail para: brasilata@brasilata.com.br
              14. CARAMURU ALIMENTOS enviar e-mail para: rh@caramuru.com
              15. CARGILL enviar e-mail para: recrutamentocargill@cargill.com
              16. CCE enviar e-mail para: rh@cce.com.br
              17. CIMENTO ITAÚ enviar e-mail para: talentos@cimentoitau.com.br
              18. CERÂMICA SANTANA enviar para: rh@ceramicasantana.com.br
              19. DELL enviar e-mail para: brasilhr@dell.com
              20. DOW enviar e-mail para: recrutamento@dow.com
              21. EMBRACO enviar e-mail para: rhembraco@embraco.com.br
              2. ESTRELA enviar e-mail para: dpessoal@estrela.ind.br
              23. FORD enviar e-mail para: selecao@ford.com
              24. GEMINI enviar e-mail para: rh@gemini.com.br
              25. GERDAU enviar e-mail para: rh-sp@gerdau.com.br
              26. GOODYEAR enviar e-mail para: recrutamento.amplant@goodyear.com
              27. GRADIENTE enviar e-mail para: rh@gradiente.com.br
              28. GRUPO ÁUREA enviar e-mail para: cv@grupoaurea.com.br
              29. INTELBRAS enviar e-mail para: rh@intelbras.com.br
              30. ITAMBÉ enviar e-mail para: rh@itambe.com.br
              31. KLABIN enviar e-mail para: recrutamento@klabin.com.br
              32. KOLUMBUS enviar e-mail para: rh-kb@kolumbus.com.br
              33. LUPO enviar e-mail para: rh@lupo.com.br
              34. MANAH enviar e-mail para: mercado@manah.com.br
              35. MARCOPOLO enviar e-mail para: inovarh@inovarh.com.br
              36. MOCOCA enviar e-mail para: rh@mococasa.com.br
              37. MONSANTO enviar e-mail para: talentos.novos@monsanto.com
              38. MOORE enviar e-mail para: selecao@moore.com.br
              39. MOSANE enviar e-mail para: rh@mosane.com.br
              40. OTIS enviar e-mail para: selecao@otis.com
              41. PANAMCO enviar e-mail para: bancodecurriculos@panamco.com.br
              42. PANCO enviar e-mail para: selfab@panco.com.br
              43. PERDIGÃO enviar e-mail para: rhvda@perdigao.com.br
              44. PROBEL enviar e-mail para: drh@probel.com.br
              45. SABÓIA enviar e-mail para: selecao@saboia.com.br
              46. SANTISTA TÊXTIL enviar e-mail para: selecao@santistatextil.com.br
              47. SCANIA enviar e-mail para: curriculo.br@scania.com
              48. SCHINCARIOL enviar e-mail para: rh@schincariol.com.br
              49. SKOL enviar e-mail para: gente@skol.com.br
              50. SONY enviar e-mail para: sonyrh@ssp.br.sony.com
              51. SONY MUSIC enviar e-mail para: talentos@sonymusic.com.br
              52. SPRINGER CARRIER enviar e-mail para: rh.springer@carrier.utc.com
              53. TECIDOS ELIZABETH enviar e-mail para: selecao@elizabeth.com.br
              54. TETRAPAK enviar e-mail para: recrutamento@tetrapak.com
              55. VICUNHA enviar e-mail para: selecao@elizabeth.com.br
              56. WICKBOLD enviar e-mail para: selecao@wickbold.com.br`


  const reg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g
  var extract = text.match(reg)
  console.log(extract);
