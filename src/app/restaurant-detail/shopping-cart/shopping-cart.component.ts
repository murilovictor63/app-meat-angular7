import { Component, OnInit } from '@angular/core';
import { ShoppingCartService } from "app/restaurant-detail/shopping-cart/shopping-cart.service";
import { CartItem } from "app/restaurant-detail/shopping-cart/cart-item.model";
import { MenuItem } from "app/restaurant-detail/menu-item/menu-item-model";
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations'
@Component({
  selector: 'mt-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  //preserveWhitespaces: true, // preserva os espaços em branco gerado pelo bootstrap, Foi inserido no arquivo main.js em nivel de aplicação
  animations: [
    trigger('row', [
      state('ready', style({opacity: 1})),
      transition('void => ready', animate('500ms 0s ease-in', keyframes([
        style({opacity: 0, transform: 'translateX(-30px)', offset: 0 }),
        style({opacity: 0.8, transform: 'translateX(10px)', offset: 0.8 }),
        style({opacity: 1, transform: 'translateX(0px)', offset: 1 })
      ]))),
      transition('ready => void', animate('500ms 0s ease-out', keyframes([
        style({opacity: 1, transform: 'translateX(0px)', offset: 0}),
        style({opacity: 0.8, transform: 'translateX(-10px)', offset: 0.2 }),
        style({opacity: 0, transform: 'translateX(30px)', offset: 1 })
      ])))
    ])
  ]
})
export class ShoppingCartComponent implements OnInit {

  constructor(private shoppingCartService: ShoppingCartService) { }

  ngOnInit() {
  }

  rowState = 'ready'

  itens(): any[]{
    return this.shoppingCartService.itens
  }

  total(): number{
    return this.shoppingCartService.total()
  }

  clear(){
    this.shoppingCartService.clearAll()
  }

  removeItem(item: CartItem){
    this.shoppingCartService.removeItem(item)
  }

  addItem(item: MenuItem){
    this.shoppingCartService.addItem(item)
  }

}
