import { CanLoad, Route, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { LoginService } from "app/security/login/login.service";

@Injectable()
export class LoggedInGuard implements CanLoad, CanActivate {

  constructor(private loginService: LoginService) { }

  canLoad(route: Route): boolean | Observable<boolean> | Promise<boolean> {
    console.log('canLoad')
    console.log(route)
    return this.checkAuthentication(route.path)
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    console.log('canActivate')
    console.log(route)
    return this.checkAuthentication(route.routeConfig.path)
  }

  checkAuthentication(path: string): boolean {
    const loggedIn = this.loginService.isLoggedIn()
    if(!loggedIn){
      this.loginService.handleLogin(`/${path}`)
    }
    return loggedIn
  }

}
